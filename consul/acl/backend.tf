terraform {
  backend "consul" {
    address = "{{ host }}"
    scheme  = "{{ scheme }}"
    path    = "{{ path }}"
    access_token   = "{{ access_token }}"
  }
}
