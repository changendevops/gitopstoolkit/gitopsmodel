module "acl-pro-w-{{ product_write.org }}-{{ product_write.product }}" {
  source  = "./modules/acl-product"
  org = "{{ product_write.org }}"
  product = "{{ product_write.product }}"
  action = "write"
}
