module "acl-org-r-{{ org_write.org }}" {
  source  = "./modules/acl-org"
  org = "{{ org_write.org }}"
  action = "write"
}
