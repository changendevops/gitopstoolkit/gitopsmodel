module "acl-org-r-{{ org_read.org }}" {
  source  = "./modules/acl-org"
  org = "{{ org_read.org }}"
  action = "read"
}
