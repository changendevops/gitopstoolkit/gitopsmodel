resource "consul_acl_policy" "policy" {
  name        = "org-${var.org}-${var.action}"
  rules       = <<-RULE
    node_prefix "terraform/${var.org}/" {
      policy = "${var.action}"
    }
    session_prefix "" {
      policy = "${var.action}"
    }
    RULE
}

resource "consul_acl_role" "role" {
    name = "org-${var.org}-${var.action}"
    description = "${var.action} Role for org ${var.org}"
    policies = [
        "${consul_acl_policy.policy.id}"
    ]
}

resource "consul_acl_token" "token" {
  description = "${var.action} token for org ${var.org}"
  policies = ["${consul_acl_policy.policy.name}"]
  roles = ["${consul_acl_role.role.name}"]
}