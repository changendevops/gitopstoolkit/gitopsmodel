variable "org" {
  description = "Name of the org"
  type        = string
}
variable "action" {
  description = "Name of the action"
  type        = string
}
variable "product" {
  description = "Name of the product"
  type        = string
}