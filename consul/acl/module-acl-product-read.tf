module "acl-pro-r-{{ product_read.org }}-{{ product_read.product }}" {
  source  = "./modules/acl-product"
  org = "{{ product_read.org }}"
  product = "{{ product_read.product }}"
  action = "read"
}
